#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/sleep.h>

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

#include "usart.h"

// number of samples in every channel
#define BUFSIZE 256

#define CHANNEL_SIZE 4

// value is calculated as follows: (TIMER_CLK / SAMPLING_RATE) - 1, for 11.5 kHz approx. 1391
#define TCC1_PER_VALUE 1390

// array that contains the samples of each channel
volatile int16_t buffer0[BUFSIZE];
volatile int16_t buffer1[BUFSIZE];
volatile int16_t buffer2[BUFSIZE];
volatile int16_t buffer3[BUFSIZE];

volatile uint16_t sample_time[BUFSIZE];

// pointer to buffer during sampling
volatile uint16_t buffer_idx = 0;

/*
    The XMegaA1 has two ADCs, ADCA and ADCB.
    Each ADC has four different result registers; The MUX selection is the individual input selection for the signal.
    Each result register and MUX selection pair is an ADC CHANNEL.
*/

// ADC MUX channels for signals, (de)selected signal source
uint8_t  mux_signals[5]  = {ADC_CH_MUXPOS_PIN1_gc, ADC_CH_MUXPOS_PIN2_gc, ADC_CH_MUXPOS_PIN3_gc, ADC_CH_MUXPOS_PIN4_gc, ADC_CH_MUXPOS_PIN5_gc};
uint8_t  g_skip_channel = 4;

// initializes ADCA channels 0-3 to sampling channels
void sample_channels_init (void) {
    // re-apply ADC settings since they are changed when calling "sample_V0"
    ADCA.CTRLA     = ADC_ENABLE_bm; // enables the ADC
    ADCA.CTRLB     = ADC_RESOLUTION_LEFT12BIT_gc | ADC_CONMODE_bm;   // 12 bit conversion, signed
    ADCA.REFCTRL   = ADC_REFSEL_INT1V_gc | ADC_BANDGAP_bm;           // internal bandgap reference

    // initialize all ADC channels
    ADC_CH_t *channel[CHANNEL_SIZE] = {&ADCA.CH0, &ADCA.CH1, &ADCA.CH2, &ADCA.CH3};
    uint8_t   muxpos[CHANNEL_SIZE]  = {};

    uint8_t j = 0;
    for (uint8_t i=0; i<CHANNEL_SIZE; ++i) {
        if (j == g_skip_channel) {
            ++j;
        }
        muxpos[i] = mux_signals[j++];
    }

    // configure all ADC channels
    for (uint8_t i=0; i<CHANNEL_SIZE; ++i) {
        channel[i]->CTRL    = ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_DIFF_gc; // differential input signal with NO GAIN
        channel[i]->MUXCTRL = muxpos[i] | ADC_CH_MUXNEG_PIN0_gc; // defines the input source for the channel
        channel[i]->CTRL   |= ADC_CH_START_bm; // trigger conversion since first conversion is usually corrupted
    }
    _delay_ms(10); // wait for ADC to stabilize and first conversion to finish
}

void polling_sample(void) {
    buffer_idx = 0;
    sample_channels_init();

    TCC0.CNT = 0;
    TCC0.CTRLA = TC_CLKSEL_DIV8_gc; // system clock divided by 8

    for (uint16_t buffer_idx = 0; buffer_idx < BUFSIZE; buffer_idx++) {
        ADCA.CH0.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
        ADCA.CH0.CTRL     |= ADC_CH_START_bm;       // start conversion
        ADCA.CH1.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
        ADCA.CH1.CTRL     |= ADC_CH_START_bm;       // start conversion
        ADCA.CH2.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
        ADCA.CH2.CTRL     |= ADC_CH_START_bm;       // start conversion
        ADCA.CH3.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
        ADCA.CH3.CTRL     |= ADC_CH_START_bm;       // start conversion

        while (!(ADCA.CH0.INTFLAGS & ADC_CH_CHIF_bm)) {}    // wait for conversion to complete
        buffer0[buffer_idx] = ADCA.CH0.RES;

        while (!(ADCA.CH1.INTFLAGS & ADC_CH_CHIF_bm)) {}    // wait for conversion to complete
        buffer1[buffer_idx] = ADCA.CH1.RES;

        while (!(ADCA.CH2.INTFLAGS & ADC_CH_CHIF_bm)) {}    // wait for conversion to complete
        buffer2[buffer_idx] = ADCA.CH2.RES;

        while (!(ADCA.CH3.INTFLAGS & ADC_CH_CHIF_bm)) {}    // wait for conversion to complete
        buffer3[buffer_idx] = ADCA.CH3.RES;

        sample_time[buffer_idx] = TCC0.CNT;
        PORTE.OUT = ~buffer_idx;
    }
}

/*
Basic sampling functionality
=============================

1. Setup timer to trigger with an frequency of the desired sampling rate
2. Create event source: When TCC1 overflow occurs notify event channel 0
3. Link ADCA to event network to perform a synchronized sweep over all three channels when event channel 0 is triggered
4. Enable interrupt for channel 3 if conversion is complete
5. In ISR: copy data from ADC channel registers to buffer, increment num_samples variable
TODO: (optional): use DMA instead of copying to memory over CPU
*/
void sample(void) {
    buffer_idx = 0;
    sample_channels_init();

    // link ADC to event network
    ADCA.EVCTRL = ADC_SWEEP_0123_gc | ADC_EVSEL_0123_gc | ADC_EVACT_SYNCHSWEEP_gc; // include all 4 channels in a channel sweep, only ONE synchronized sweep

    ADCA.INTFLAGS = 15; // clears interrupt flags manually for all ADC channels

    // select the event source for event channel 0
    EVSYS.CH0MUX = EVSYS_CHMUX_TCC1_OVF_gc;

    // TCC1 settings
    // config TCC1 so that an overflow is triggered with DESIRED SAMPLING RATE
    TCC1.PER     = TCC1_PER_VALUE;
    TCC1.CTRLA   = TC_CLKSEL_DIV1_gc;

    ADCA.CH3.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_LO_gc; // enable low level interrupts for channel 3 when ADC conversion is complete

    // TCC0 settings, for counting the elapsed time over all samples
    TCC0.CNT = 0;
    TCC0.CTRLA = TC_CLKSEL_DIV8_gc;

    // wait for sample buffer to fill
    while (buffer_idx != BUFSIZE);

    // stop timer
    TCC0.CTRLA = TC_CLKSEL_OFF_gc;
    TCC1.CTRLA = TC_CLKSEL_OFF_gc;
}

ISR (ADCA_CH3_vect) {
    int16_t buffer_idx_copy = buffer_idx;

    sample_time[buffer_idx_copy] = TCC0.CNT;
    buffer0[buffer_idx_copy] = ADCA.CH0.RES;
    buffer1[buffer_idx_copy] = ADCA.CH1.RES;
    buffer2[buffer_idx_copy] = ADCA.CH2.RES;
    buffer3[buffer_idx_copy] = ADCA.CH3.RES;

    ++buffer_idx_copy;

    if (buffer_idx_copy == BUFSIZE) {
        ADCA.CH3.INTCTRL = 0; // disable interrupts for channel 3
    }

    buffer_idx = buffer_idx_copy;
}

// liest die Bezugsspannung and A:0 ein, sollte in etwa Vcc/2 = 1,65 V betragen
int sample_V0(void) {
    // A:0 wird als A_ref verwendet und damit die interne 1-Volt-Referenz messen
    ADCA.CTRLB     = ADC_RESOLUTION_12BIT_gc | ADC_CONMODE_bm;   // 12 bit conversion, signed
    ADCA.REFCTRL   = ADC_REFSEL_AREFA_gc | ADC_BANDGAP_bm;       // internal bandgap reference

    ADCA.CH0.CTRL    = ADC_CH_INPUTMODE_INTERNAL_gc;
    ADCA.CH0.MUXCTRL = ADC_CH_MUXINT_BANDGAP_gc;
    ADCA.CTRLA      |= ADC_ENABLE_bm;
    // 2x wandeln, erstes Ergebnis verwerfen
    for (uint8_t i=0; i<2; i++) {
        ADCA.CH0.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
        ADCA.CH0.CTRL |= ADC_CH_START_bm;       // start conversion
        while (!(ADCA.CH0.INTFLAGS & ADC_CH_CHIF_bm)) {}    // wait for conversion to complete
    }
    // A_ref * ( x / 2048) = 1 Volt
    return ADCA.CH0.RES;
}

/*
void sample(void) {
    sample_channels_init();
    // setup DMA
    DMA.CTRL      = DMA_ENABLE_bm;

    DMA.CH0.ADDRCTRL = DMA_CH_SRCRELOAD_NONE_gc | DMA_CH_SRCDIR_FIXED_gc| DMA_CH_DESTRELOAD_NONE_gc | DMA_CH_DESTDIR_INC_gc;
    DMA.CH0.TRIGSRC  = DMA_CH_TRIGSRC_ADCA_CH0_gc;  // ADCA channel 0 triggers DMA transfer
    DMA.CH0.TRFCNT   = 1;                           // 1 byte per transfer
    DMA.CH0.SRCADDR0 = ((uint16_t)buffer0) & 0xff;              // low-byte address of buffer
    DMA.CH0.SRCADDR1 = (((uint16_t)buffer0)>>8) & 0xff;         // high-byte
    DMA.CH0.REPCNT   = 255;                         // repeat for 255 transfers
    DMA.CH0.CTRLA    = DMA_CH_REPEAT_bm | DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;
    DMA.CH0.CTRLB   |= DMA_CH_TRNIF_bm; // reset transmission complete flag

    DMA.CH1.ADDRCTRL = DMA_CH_SRCRELOAD_NONE_gc | DMA_CH_SRCDIR_FIXED_gc| DMA_CH_DESTRELOAD_NONE_gc | DMA_CH_DESTDIR_INC_gc;
    DMA.CH1.TRIGSRC  = DMA_CH_TRIGSRC_ADCA_CH1_gc;
    DMA.CH1.TRFCNT   = 1;
    DMA.CH1.SRCADDR0 = ((uint16_t)buffer1) & 0xff;      // low-byte
    DMA.CH1.SRCADDR1 = (((uint16_t)buffer1)>>8) & 0xff; // high-byte
    DMA.CH1.REPCNT   = 255;
    DMA.CH1.CTRLA    = DMA_CH_REPEAT_bm | DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;
    DMA.CH1.CTRLB   |= DMA_CH_TRNIF_bm; // reset transmission complete flag

    PORTE.OUTCLR = 0x0f; // 4 LEDs on

    // enable DMA channels
    DMA.CH0.CTRLA |= DMA_ENABLE_bm;
    DMA.CH1.CTRLA |= DMA_ENABLE_bm;

    // and start ADC
    ADCA.CTRLB     |= ADC_FREERUN_bm;
    ADCA.EVCTRL     = ADC_SWEEP_01_gc;
    ADCA.CTRLA     |= ADC_FLUSH_bm;          // ADC pipeline flush

    ADCA.CH0.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
    ADCA.CH0.CTRL |= ADC_CH_START_bm;       // start conversion
    ADCA.CH1.INTFLAGS |= ADC_CH_CHIF_bm;    // clear interrupt flag
    ADCA.CH1.CTRL |= ADC_CH_START_bm;       // start conversion

    // wait until DMA is finished
    while (!(DMA.CH0.CTRLB & DMA_CH_TRNIF_bm) || !(DMA.CH1.CTRLB & DMA_CH_TRNIF_bm)) {};

    ADCA.CTRLA &= ~ADC_ENABLE_bm;
    ADCA.CTRLB     &= ~ADC_FREERUN_bm;
    PORTE.OUTCLR = 0xff; // LEDs off
}
*/
//  https://www.kampis-elektroecke.de/avr/xmega-dma/

// reads signature byte for accessing µC's calibration data
uint8_t readSignatureByte(uint16_t Address) {
    NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
    uint8_t Result;
    Result = pgm_read_byte((uint8_t *)Address);
    NVM_CMD = NVM_CMD_NO_OPERATION_gc;
    return Result;
}

// initialize ADC on port A
void adc_init(void) {
    ADCA.CTRLB     = ADC_RESOLUTION_LEFT12BIT_gc | ADC_CONMODE_bm;   // 8 bit conversion, signed
    ADCA.REFCTRL   = ADC_REFSEL_INT1V_gc | ADC_BANDGAP_bm;  // internal bandgap reference
    ADCA.PRESCALER = ADC_PRESCALER_DIV8_gc; // sets the ADC clock rate to fracture of system clock

    // calibrate ADCA
    ADCA.CALL = readSignatureByte(offsetof(NVM_PROD_SIGNATURES_t, ADCACAL0)); //ADC Calibration Byte 0
    ADCA.CALH = readSignatureByte(offsetof(NVM_PROD_SIGNATURES_t, ADCACAL1)); //ADC Calibration Byte 1
}

int main(void) {
    // init LEDs and turn them off
    PORTE.OUTSET = 0xff;
    PORTE.DIRSET = 0xff;

    // set clock to 16 Mhz
    OSC.PLLCTRL = OSC_PLLSRC_RC2M_gc | 8;
    OSC.CTRL   |= OSC_PLLEN_bm;

    while (!(OSC.STATUS & OSC_PLLRDY_bm)) {} ; // wait for PLL to lock
    CCP = CCP_IOREG_gc;
    CLK.CTRL = CLK_SCLKSEL_PLL_gc;

    // Show reason for reset for 500ms
    uint8_t stat  = RST.STATUS;
    RST.STATUS    = 63;
    PORTE.OUT     = ~stat;
    _delay_ms(500);
    PORTE.OUTSET  = 0xff;

    // enable all interrupt levels
    PMIC.CTRL |= PMIC_LOLVLEX_bm | PMIC_MEDLVLEX_bm | PMIC_HILVLEX_bm;
    sei(); // enable IRQs globally

    usart_init();
    adc_init();

    usart_puts_P(SERIAL,"Hello World!\n");
    usart_puts_P(SERIAL, "\nReason for reset: ");
    if (stat & 32) usart_puts_P(SERIAL, "software ");
    if (stat & 16) usart_puts_P(SERIAL, "PDI ");
    if (stat & 8) usart_puts_P(SERIAL, "watchdog ");
    if (stat & 4) usart_puts_P(SERIAL, "brown-out ");
    if (stat & 2) usart_puts_P(SERIAL, "external ");
    if (stat & 1) usart_puts_P(SERIAL, "power-on");
    usart_putc(SERIAL, '\n');

    while (true) {
        char c = usart_getc(SERIAL);
        switch (c) {
            case 's':
                sample();
                usart_puts_P(SERIAL, "\nsampling finished, TCC0=");
                usart_putDec(SERIAL, TCC0.CNT);
                usart_putc(SERIAL, '\n');
                break;

            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
                g_skip_channel = c - '1';
                usart_puts_P(SERIAL, "\nsampling all channels except ");
                usart_putDec(SERIAL, g_skip_channel);
                usart_putc(SERIAL, '\n');
                break;

            case 'p':
                usart_putc(SERIAL, '\n');
                for (uint16_t i=0; i<BUFSIZE; i++) {
                    usart_putDec(SERIAL, i);
                    usart_putc(SERIAL, ' ');
                    usart_putDec(SERIAL, sample_time[i]);
                    usart_putc(SERIAL, ' ');
                    usart_putDec(SERIAL, buffer0[i]);
                    usart_putc(SERIAL, ' ');
                    usart_putDec(SERIAL, buffer1[i]);
                    usart_putc(SERIAL, ' ');
                    usart_putDec(SERIAL, buffer2[i]);
                    usart_putc(SERIAL, ' ');
                    usart_putDec(SERIAL, buffer3[i]);
                    usart_putc(SERIAL, '\n');
                }
                break;

            case 'c':
                for (uint16_t i=0; i<BUFSIZE; i++) {
                    buffer0[i] = 0;
                    buffer1[i] = 0;
                    buffer2[i] = 0;
                    buffer3[i] = 0;
                    sample_time[i] = 0;
                }
                usart_puts_P(SERIAL, "\nBuffer cleared.\n");
                break;

            case 0: // toggle LED when no character was received
                PORTE.OUTTGL = 1;
                break;

            case 'r': // trigger reset
                CCP = CCP_IOREG_gc;
                RST.CTRL = RST_SWRST_bm;
                break;

            case 'v': // sample v_0
                usart_puts_P(SERIAL, "\nVref=");
                usart_putDec(SERIAL, sample_V0());
                usart_putc(SERIAL, '\n');
                break;

            default:
                break;
        }
        _delay_ms(250);
    }
}
