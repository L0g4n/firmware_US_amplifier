# Firmware US Amplifier

This repository provides the firmware code for the AVR atxmega128a1 on the XMEGA-A1 Xplained evaluation kit which samples four connected microphones 
and uploads the data via USART on the serial interface.